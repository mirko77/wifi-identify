# Wifi Identify #

Simple application to show the Wifi network name (SSID) when the device is connected in the notification panel.

Other information are optionally shown: IP address and speed in Mbps

It is useful for those ROMs which do not show it out of the box, like the Galaxy S2 on ICS.

[Google Play](https://play.google.com/store/apps/details?id=com.eezzyweb.wifiidentify)